# XMLB

**XML** but with **B**rackets

# What

```xml
<root>
  <foo1>Some text</foo1>
  <bar thing="4"/>

  <!-- Comment 1 -->
  <!-- Comment 2 -->

  <baz foo1="bar1" foo2="bar2" foo3="3">
    <foo2>
      Some more text

      <!--
        Multi
        Line
        Comment
      -->
    </foo2>
  </baz>
</root>
```

Becomes

```
<root> {
  <foo1> { Some text }
  <bar thing="4">

  // Comment 1
  // Comment 2

  <baz foo1="bar1" foo2="bar2" foo3="3"> {
    <foo2> {
      Some more text

      /*
        Multi
        Line
        Comment
      */
    }
  }
}
```

**XML** But with **B**rackets!

# How

```rust
use xmlb::XMLBFile;

fn main() {
    let source: String = xmlb_source_code;

    let xmlb_file = XMLBFile::from_string(source);

    // Convert to xml
    let xml = xmlb_file.to_xml(true); // true or false to keep comments or not
}
```

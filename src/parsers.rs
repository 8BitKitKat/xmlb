use nom::{
    branch::alt,
    bytes::complete::{escaped, is_a, is_not, tag, take_until},
    character::complete::{char, multispace0, none_of, one_of},
    combinator::{all_consuming, cut, map, opt},
    error::context,
    multi::many0,
    sequence::{delimited, pair, tuple},
};

#[derive(Debug, Clone, PartialEq)]
pub struct XMLBFile {
    pub doctype: Option<String>,
    pub body: Vec<BodyItem>,
}

impl XMLBFile {
    /// Parse a string into an xmlb file
    pub fn from_string(mut string: String) -> Result<Self, String> {
        string.push('\n');
        let s = string.as_str();

        match parse_file(s) {
            Ok(s) => Ok(s.1),
            Err(e) => {
                let verbose = match e {
                    nom::Err::Error(e) => e,
                    nom::Err::Failure(e) => e,

                    // None of these parsers should be able to return incomplete
                    nom::Err::Incomplete(_) => unreachable!(),
                };
                Err(nom::error::convert_error(s, verbose))
            }
        }
    }

    /// Convert the xmlb "ast" into xml
    pub fn to_xml(&self, keep_comments: bool) -> String {
        let mut buf = String::new();
        buf.push_str("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");

        if let Some(doctype) = &self.doctype {
            buf.push_str(&format!("<!DOCTYPE {}>", doctype));
        }

        buf.push_str(&Self::convert_body(&self.body, keep_comments));

        buf.pop();
        buf
    }

    fn convert_body(body: &Vec<BodyItem>, keep_comments: bool) -> String {
        let mut buf = String::new();

        for item in body {
            buf.push_str(&match item {
                BodyItem::Tag(t) => Self::convert_tag(t, keep_comments),
                BodyItem::Text(t) => t.clone(),

                BodyItem::Comment(t) if keep_comments => format!("<!--{} -->", t),
                // BodyItem::MultilineComment(t) if keep_comments => format!("<!--{} -->", t),
                _ => "".to_string(),
            });
        }

        buf
    }

    fn convert_tag(tag: &Tag, keep_comments: bool) -> String {
        let mut buf = String::new();

        if let Some(body) = &tag.body {
            buf.push_str(&Self::convert_tag_values(tag, ">"));
            buf.push_str(&Self::convert_body(body, keep_comments));
            buf.push_str(&format!("</{}>", tag.name));
        } else {
            buf.push_str(&Self::convert_tag_values(tag, "/>"));
        }

        buf
    }

    fn convert_tag_values(tag: &Tag, suffix: &'static str) -> String {
        let mut buf = String::new();

        buf.push_str(&format!("<{}", tag.name));
        if let Some(values) = &tag.values {
            buf.push(' ');
            for value in values.iter() {
                buf.push_str(&format!("{}=\"{}\" ", value.0, value.1));
            }
            buf.pop();
        }
        buf.push_str(suffix);

        buf
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Tag {
    pub name: String,
    pub values: Option<Vec<(String, String)>>,
    pub body: Option<Vec<BodyItem>>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum BodyItem {
    Tag(Tag),
    Comment(String),
    Text(String),
}

type IResult<I, O> = nom::IResult<I, O, nom::error::VerboseError<I>>;

pub fn parse_file(s: &str) -> IResult<&str, XMLBFile> {
    let (s, doctype) = opt(delimited(
        tuple((tag("<!"), multispace0, tag("DOCTYPE"), multispace0)),
        ident,
        pair(multispace0, char('>')),
    ))(s)?;

    let (s, body) = all_consuming(many0(body_item))(s)?;

    Ok((s, XMLBFile { doctype, body }))
}

pub fn parse_tag(s: &str) -> IResult<&str, Tag> {
    let (s, _) = char('<')(s)?;
    let (s, _) = multispace0(s)?;
    let (s, name) = ident(s)?;
    let (s, _) = multispace0(s)?;
    let (s, values) = map(
        many0(map(tuple((multispace0, binding, multispace0)), |s| s.1)),
        |s| {
            if s.is_empty() {
                None
            } else {
                Some(s)
            }
        },
    )(s)?;
    let (s, _) = char('>')(s)?;

    let (s, has_body) = map(opt(tuple((multispace0, char('{')))), |s| s.is_some())(s)?;

    if has_body {
        let (s, body) = map(cut(many0(body_item)), |s| {
            if s.is_empty() {
                None
            } else {
                Some(s)
            }
        })(s)?;
        let (s, _) = char('}')(s)?;

        Ok((s, Tag { name, values, body }))
    } else {
        Ok((
            s,
            Tag {
                name,
                values,
                body: None,
            },
        ))
    }
}

pub fn body_item(s: &str) -> IResult<&str, BodyItem> {
    alt((
        map(parse_tag, |s| BodyItem::Tag(s)),
        map(comment, |s| BodyItem::Comment(s.to_string())),
        map(multiline_comment, |s| BodyItem::Comment(s.to_string())),
        map(text, |s| BodyItem::Text(s.to_string())),
    ))(s)
}

pub fn text(s: &str) -> IResult<&str, &str> {
    is_not("<>{}/**/")(s)
}

pub fn comment(s: &str) -> IResult<&str, &str> {
    let (s, _) = tag("//")(s)?;
    take_until("\n")(s)
}

pub fn multiline_comment(s: &str) -> IResult<&str, &str> {
    delimited(tag("/*"), take_until("*/"), tag("*/"))(s)
}

pub fn ident(s: &str) -> IResult<&str, String> {
    context(
        "ident",
        map(
            is_a(r#"qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890_"#),
            |s: &str| s.to_string(),
        ),
    )(s)
}

pub fn binding(s: &str) -> IResult<&str, (String, String)> {
    context(
        "binding",
        map(
            tuple((
                /* ident */ ident,
                multispace0,
                cut(char('=')),
                multispace0,
                /* val */ cut(string),
            )),
            |s| (s.0, s.4),
        ),
    )(s)
}

pub fn string(s: &str) -> IResult<&str, String> {
    context(
        "string",
        map(
            delimited(
                tag("\""),
                alt((escaped(none_of(r#"\""#), '\\', one_of(r#"nrt"\"#)), tag(""))),
                tag("\""),
            ),
            |s: &str| s.to_string(),
        ),
    )(s)
}

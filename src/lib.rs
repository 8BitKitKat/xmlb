pub mod parsers;

pub use parsers::XMLBFile;

#[cfg(test)]
pub mod tests {
    use super::*;

    const SRC: &str = r#"<!DOCTYPE SOME_DOC_TYPE>
    
    <root> {
        Some text
        
        <foo bar="2">
        
        // Comment
        
        <bar foo="bar"> {
            <baz> {
                Some more text
                
                /*
                  muliline
                  comment
                */
            }
        }
    }"#;

    #[test]
    fn test() {
        use parsers::{BodyItem, Tag};

        let xmlb_file = XMLBFile::from_string(SRC.to_string()).unwrap();

        assert_eq!(
            xmlb_file,
            XMLBFile {
                doctype: Some("SOME_DOC_TYPE".to_string()),
                body: vec![
                    BodyItem::Text("\n    \n    ".to_string()),
                    BodyItem::Tag(Tag {
                        name: "root".to_string(),
                        values: None,
                        body: Some(vec![
                            BodyItem::Text("\n        Some text\n        \n        ".to_string()),
                            BodyItem::Tag(Tag {
                                name: "foo".to_string(),
                                values: Some(vec![("bar".to_string(), "2".to_string())]),
                                body: None,
                            }),
                            BodyItem::Text("\n        \n        ".to_string()),
                            BodyItem::Comment(" Comment".to_string()),
                            BodyItem::Text("\n        \n        ".to_string()),
                            BodyItem::Tag(Tag {
                                name: "bar".to_string(),
                                values: Some(vec![("foo".to_string(), "bar".to_string())]),
                                body: Some(vec![
                                    BodyItem::Text("\n            ".to_string()),
                                    BodyItem::Tag(Tag {
                                        name: "baz".to_string(),
                                        values: None,
                                        body: Some(vec![
                                            BodyItem::Text("\n                Some more text\n                \n                ".to_string()),
                                            BodyItem::Comment("\n                  muliline\n                  comment\n                ".to_string()),
                                            BodyItem::Text("\n            ".to_string()),
                                        ])
                                    }),
                                    BodyItem::Text("\n        ".to_string()),
                                ])
                            }),
                            BodyItem::Text("\n    ".to_string()),
                        ])
                    }),
                    BodyItem::Text("\n".to_string()),
                ]
            }
        );

        let xml_with_comments = xmlb_file.to_xml(true);
        assert_eq!(xml_with_comments, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE SOME_DOC_TYPE>\n    \n    <root>\n        Some text\n        \n        <foo bar=\"2\"/>\n        \n        <!-- Comment -->\n        \n        <bar foo=\"bar\">\n            <baz>\n                Some more text\n                \n                <!--\n                  muliline\n                  comment\n                 -->\n            </baz>\n        </bar>\n    </root>".to_string(), );

        let xml_without_comments = xmlb_file.to_xml(false);
        assert_eq!(xml_without_comments, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE SOME_DOC_TYPE>\n    \n    <root>\n        Some text\n        \n        <foo bar=\"2\"/>\n        \n        \n        \n        <bar foo=\"bar\">\n            <baz>\n                Some more text\n                \n                \n            </baz>\n        </bar>\n    </root>".to_string());
    }
}
